package mongodb

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/golang-libs/databases.git/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"time"
)

var DB *mongo.Client

type MongoExecutorFunc func(extSession mongo.SessionContext) (interface{}, error)

func EstablishNewDB(dbconf config.DBConfig) (*mongo.Client, error) {
	var err error
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	//opts := options.ClientOptions{
	//	Hosts: []string{
	//		"mongodb://%s:%s", dbconf.Host, dbconf.Port,
	//	},
	//	Auth: &options.Credential{
	//		Username:    dbconf.User,
	//		Password:    dbconf.Pass,
	//		PasswordSet: dbconf.Pass != "",
	//	},
	//}
	d, err := mongo.Connect(ctx, options.Client().
		ApplyURI(fmt.Sprintf("mongodb://%s:%s", dbconf.Host, dbconf.Port)))
	//mongo.Connect(ctx, &opts)
	if err != nil {
		return nil, err
	}

	ctx, _ = context.WithTimeout(context.Background(), 3*time.Second)
	err = d.Ping(ctx, readpref.Primary())
	if err != nil {
		return nil, err
	}

	return d, nil
}

func EstablishDefaultDB(dbconf config.DBConfig) (err error) {
	DB, err = EstablishNewDB(dbconf)
	return
}

func initSessOnDB(extTx *mongo.Session, extDB *mongo.Client) (sess mongo.Session, err error, externalTrn bool) {
	externalTrn = extTx != nil
	if externalTrn {
		return *extTx, nil, externalTrn
	} else {
		sess, err = extDB.StartSession()
		//if err != nil {
		//	return nil, errors.Wrap(err, ""), false
		//}
		//err = sess.StartTransaction()
		return
	}
}

func Do(f MongoExecutorFunc, extSession *mongo.Session) (intf interface{}, err error) {
	// Инициализируем новую транзакцию, если мы не в существующей
	sess, err, extSess := initSessOnDB(extSession, DB)
	if err != nil {
		return nil, errors.Wrap(err, "")
	}
	ctx, _ := context.WithTimeout(context.Background(), 3*time.Second)
	err = mongo.WithSession(ctx, sess, func(sc mongo.SessionContext) error {
		intf, err = f(sc)
		return err
	})
	// Откат при ошибке
	if err != nil {
		//_ = sess.AbortTransaction(DefaultContext)
		return intf, errors.Wrap(err, "")
	}

	// Коммит, если все норм и мы начинали транзакцию
	if !extSess {
		//err = sess.CommitTransaction(DefaultContext)
		//if err != nil {
		//	return nil, errors.Wrap(err, "")
		//}
		ctx, _ = context.WithTimeout(context.Background(), 3*time.Second)
		sess.EndSession(ctx)
	}

	return
}
