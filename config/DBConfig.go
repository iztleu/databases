package config

import (
	"fmt"
	"net/url"
)

type DBConfig struct {
	Host            string   `json:"host"`
	Hosts           []string `json:"hosts"`
	Port            string   `json:"port"`
	User            string   `json:"user"`
	Pass            string   `json:"pass"`
	Name            string   `json:"name"`
	IdleConns       int      `json:"idle_conns"`
	OpenConns       int      `json:"open_conns"`
	DriverName      string   `json:"driver_name"`
	ApplicationName string   `json:"app_name"`
}

func (dbc *DBConfig) GetConnectionStr() string {
	switch dbc.DriverName {
	case "oci8", "godror":
		return fmt.Sprintf("%s/%s@%s:%s/%s", dbc.User, dbc.Pass, dbc.Host, dbc.Port,
			dbc.Name)
	case "mysql":
		user := url.QueryEscape(dbc.User)
		pass := url.QueryEscape(dbc.Pass)
		return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", user, pass, dbc.Host, dbc.Port,
			dbc.Name)
	case "sqlserver":
		user := url.QueryEscape(dbc.User)
		pass := url.QueryEscape(dbc.Pass)
		return fmt.Sprintf("sqlserver://%s:%s@%s:%s?database=%s&encrypt=disable", user, pass, dbc.Host, dbc.Port,
			dbc.Name)
	case "pgx":
		user := url.QueryEscape(dbc.User)
		pass := url.QueryEscape(dbc.Pass)
		return fmt.Sprintf("postgres://%s:%s@%s:%s/%s", user, pass, dbc.Host, dbc.Port, dbc.Name)
	default:
		user := url.QueryEscape(dbc.User)
		pass := url.QueryEscape(dbc.Pass)
		return fmt.Sprintf("postgres://%s:%s@%s:%s/%s?fallback_application_name=%s&sslmode=disable", //"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable application_name=%s",
			user, pass, dbc.Host, dbc.Port, dbc.Name, dbc.ApplicationName)
	}
}

func (dbc *DBConfig) GetTimezoneStmt() string {
	switch dbc.DriverName {
	case "oci8", "godror":
		return "alter session set time_zone = '+06:00'"
	case "mysql":
		return "SET time_zone = '+06:00'"
	case "sqlserver":
		return ";"
	default:
		return "SET TIME ZONE 'Asia/Almaty'"
	}
}
