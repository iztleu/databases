package redis

import (
	"context"
	"fmt"
	"github.com/redis/go-redis/v9"
	"gitlab.com/golang-libs/databases.git/config"
	"log"
)

var RedisClient *redis.Client

func EstablishNewDB(dbconf config.DBConfig, logger *log.Logger) (*redis.Client, error) {
	initRedisLog()
	url := fmt.Sprintf("redis://%s:%s", dbconf.Host, dbconf.Port)
	opts, err := redis.ParseURL(url)
	if err != nil {
		return nil, err
	}
	opts.Password = dbconf.Pass
	opts.MinIdleConns = dbconf.IdleConns

	client := redis.NewClient(opts)
	_, err = client.Ping(context.Background()).Result()
	if err != nil {
		return client, err
	}

	return client, nil
}

func EstablishDefaultDB(dbconf config.DBConfig, logger *log.Logger) (err error) {
	RedisClient, err = EstablishNewDB(dbconf, logger)
	return
}

func Close() {
	_ = RedisClient.Close()
}
