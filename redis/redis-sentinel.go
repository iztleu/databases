package redis

import (
	"context"
	"fmt"
	"github.com/redis/go-redis/v9"
	"gitlab.com/golang-libs/databases.git/config"
)

func EstablishNewSentinelDB(dbconf config.DBConfig) (*redis.Client, error) {
	addrs := dbconf.Hosts
	if len(addrs) == 0 {
		addrs = []string{fmt.Sprintf("%s:%s", dbconf.Host, dbconf.Port)}
	}
	initRedisLog()
	client := redis.NewFailoverClient(&redis.FailoverOptions{
		MasterName:    "mymaster",
		SentinelAddrs: addrs,
		//SentinelPassword:   dbconf.Pass,
		Password:     dbconf.Pass,
		MinIdleConns: dbconf.IdleConns,
	})
	_, err := client.Ping(context.Background()).Result()
	if err != nil {
		return client, err
	}

	return client, nil
}

func EstablishDefaultSentinelDB(dbconf config.DBConfig) (err error) {
	RedisClient, err = EstablishNewSentinelDB(dbconf)
	return
}
