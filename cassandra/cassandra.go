package cassandra

import (
	"fmt"
	"github.com/gocql/gocql"
	"github.com/pkg/errors"
	"gitlab.com/golang-libs/databases.git/config"
	"time"
)

//var db *gocql.ClusterConfig
var MainSess *gocql.Session

type CassandraExecutorFunc func(*gocql.Session) (interface{}, error)

func EstablishNewDB(dbconf config.DBConfig) (sess *gocql.Session, err error) {
	d := gocql.NewCluster(fmt.Sprintf("%s", dbconf.Host))
	d.Keyspace = dbconf.Name

	d.Consistency = gocql.Quorum
	d.ConnectTimeout = time.Second * 2
	//d.Authenticator = gocql.PasswordAuthenticator{Username: dbconf.User, Password: dbconf.Pass}

	sess, err = d.CreateSession()
	if err != nil {
		return sess, nil
	}

	return sess, nil

	//d.SetMaxIdleConns(dbconf.IdleConns)
	//d.SetMaxOpenConns(dbconf.OpenConns)
}

func EstablishDefaultDB(dbconf config.DBConfig) (err error) {
	MainSess, err = EstablishNewDB(dbconf)
	return
}

//func initSessOnDB(extSess *gocql.Session, extDB *gocql.ClusterConfig) (sess *gocql.Session, err error, externalTrn bool) {
//	externalTrn = extSess != nil
//	if externalTrn {
//		return extSess, nil, externalTrn
//	} else {
//		sess, err = extDB.CreateSession()
//		return
//	}
//}

func Do(f CassandraExecutorFunc, extSess *gocql.Session) (intf interface{}, err error) {
	// Инициализируем новую транзакцию, если мы не в существующей
	//sess, err, extS := initSessOnDB(extSess, db)
	//if err != nil {
	//	return nil, customErrors.Wrap(err, "")
	//}

	defer func() {
		if r := recover(); r != nil {
			//if !extS {
			//	sess.Close()
			//}
			switch x := r.(type) {
			case string:
				err = errors.Errorf(x, "Паника")
			case error:
				err = errors.Wrap(x, "Паника")
			default:
				err = errors.Errorf("panic", "")
			}
		}
	}()

	intf, err = f(MainSess)
	// Откат при ошибке
	if err != nil {
		return intf, errors.Wrap(err, "")
	}

	//if !extS {
	//	sess.Close()
	//}

	return
}
