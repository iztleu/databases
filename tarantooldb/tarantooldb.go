package tarantooldb

import (
	"errors"
	"fmt"
	"github.com/tarantool/go-tarantool"
	"github.com/tarantool/go-tarantool/multi"
	"gitlab.com/golang-libs/databases.git/config"
	"gopkg.in/vmihailenco/msgpack.v2"
	"log"
	"time"
)

type Cnt struct {
	Count int64
}

const dataFieldsCount = 1

func (c Cnt) EncodeMsgpack(e *msgpack.Encoder) error {
	if err := e.EncodeArrayLen(dataFieldsCount); err != nil {
		return err
	}

	if err := e.EncodeInt64(c.Count); err != nil {
		return err
	}

	return nil
}

func (c *Cnt) DecodeMsgpack(d *msgpack.Decoder) error {
	var err error
	var l int
	if l, err = d.DecodeArrayLen(); err != nil {
		return err
	}
	if l != dataFieldsCount {
		return fmt.Errorf("Client data array len doesn't match: %d", l)
	}

	if c.Count, err = d.DecodeInt64(); err != nil {
		return err
	}

	return nil
}

func Count(space, index string, values []interface{}) (int64, error) {
	r, err := Client.Call(fmt.Sprintf("box.space.%s.index.%s:count", space, index), values)
	if err != nil {
		return 0, err
	}
	// Уродливо, да
	data := r.Data[0].([]interface{})
	cnt, ok := data[0].(uint64)
	if !ok {
		return 0, errors.New("Ошибка получения количества записей для " + space + ":" + index)
	}

	return int64(cnt), nil
}

var Client *multi.ConnectionMulti

func EstablishNewDB(dbconf config.DBConfig) (*multi.ConnectionMulti, error) {
	opts := tarantool.Opts{
		Timeout:       5 * time.Second,
		Reconnect:     1 * time.Second,
		MaxReconnects: 9999,
		User:          dbconf.User,
		Pass:          dbconf.Pass,
	}

	client, err := multi.Connect(dbconf.Hosts, opts)
	if err != nil {
		log.Fatalf("Failed to connect: %s", err.Error())
	}

	r, err := client.Ping()
	if err != nil {
		log.Fatalf("Failed to ping: %s", err.Error())
	}

	if r.Error != "" {
		log.Fatalf("Tarantool error: %s", r.Error)
	}

	return client, nil
}

func EstablishDefaultDB(dbconf config.DBConfig) (err error) {
	Client, err = EstablishNewDB(dbconf)
	return
}

func Close() {
	_ = Client.Close()
}
